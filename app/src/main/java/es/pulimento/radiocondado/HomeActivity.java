package es.pulimento.radiocondado;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toolbar;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import es.pulimento.radiocondado.fragments.FacebookFragment;
import es.pulimento.radiocondado.fragments.InfoFragment;
import es.pulimento.radiocondado.fragments.PlayerControlsFragment;
import es.pulimento.radiocondado.fragments.TweetsListFragment;
import es.pulimento.radiocondado.utils.Constants;
import es.pulimento.radiocondado.utils.CustomIndeterminateProgressBar;
import es.pulimento.radiocondado.utils.IColorActionBarFragment;
import es.pulimento.radiocondado.utils.RCPagerAdapter;
import es.pulimento.radiocondado.utils.SlidingTabLayout;
import es.pulimento.radiocondado.utils.ZoomPageTransformer;

public class HomeActivity extends FragmentActivity {

    private SlidingTabLayout mSlidingTabLayout;
    private CustomIndeterminateProgressBar mProgressBar;
    private PlayerControlsFragment mPlayerControlsFragment;
    private Window mWindow;
    private BroadcastReceiver activityReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (isMyServiceRunning()) {
                // Metadata stuff
                String metadata = intent.getStringExtra(Constants.INTENT_SEND_META);
                PlayerControlsFragment controls = (PlayerControlsFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.fragment_player_controls);
                controls.modifyMetadataText(metadata);

                if (!intent.getExtras().keySet().contains(Constants.INTENT_EXTRA_IS_PREPARED))
                    return;

                // Progress bar stuff
                boolean mustShowProgressBar = intent.getBooleanExtra(Constants.INTENT_EXTRA_IS_PREPARED,
                        false);
                mProgressBar.setVisibility(mustShowProgressBar ? View.GONE : View.VISIBLE);
            }
        }
    };

    static int blendColors(int from, int to, float ratio) {
        final float inverseRation = 1f - ratio;
        final float r = Color.red(from) * ratio + Color.red(to) * inverseRation;
        final float g = Color.green(from) * ratio + Color.green(to) * inverseRation;
        final float b = Color.blue(from) * ratio + Color.blue(to) * inverseRation;
        return Color.rgb((int) r, (int) g, (int) b);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int transStatus = WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS;
        winParams.flags |= transStatus;
        win.setAttributes(winParams);

        super.onCreate(savedInstanceState);

        // Set content view and bind views
        setContentView(R.layout.activity_home);
        mProgressBar = (CustomIndeterminateProgressBar) findViewById(R.id.pb_controls_buffering);
        mProgressBar.setVisibility(View.GONE);

        // Colorize status bar
        /*
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            setTranslucentStatus(true);
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setStatusBarTintResource(R.color.primaryColor);
            tintManager.setNavigationBarTintEnabled(true);
            tintManager.setNavigationBarTintResource(R.color.primaryColor);
        }
        */

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWindow = getWindow();
            mWindow.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            mWindow.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            mWindow.setStatusBarColor(getResources().getColor(R.color.primaryColor));
            mWindow.setNavigationBarColor(getResources().getColor(R.color.primaryColor));
        }


        // Set-up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setActionBar(toolbar);

        // Set the ActionBar background
        final ColorDrawable actionBarBackground = new ColorDrawable();
        getActionBar().setBackgroundDrawable(actionBarBackground);

        // Set-up the pager adapter, add content to it
        final RCPagerAdapter pagerAdapter = new RCPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(InfoFragment.newInstance(getResources().getColor(R.color.primaryColor)));
        pagerAdapter.addFragment(TweetsListFragment.newInstance(getResources().getColor(R.color.twitter_color)));
        pagerAdapter.addFragment(FacebookFragment.newInstance(getResources().getColor(R.color.facebook_color)));

        // Set-up the pager itself
        final ViewPager mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(pagerAdapter);
        mPager.setPageTransformer(true, new ZoomPageTransformer());

        // Get player controls fragment
        mPlayerControlsFragment = (PlayerControlsFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_player_controls);

        // Set-up the pager header
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.pager_header);
        mSlidingTabLayout.setViewPager(mPager);
        mSlidingTabLayout.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                if (position >= pagerAdapter.getCount() - 1) {
                    // Guard against ArrayIndexOutOfBoundsException
                    return;
                }
                // Retrieve the current and next ColorFragment
                final IColorActionBarFragment from = (IColorActionBarFragment) pagerAdapter.getItem(position);
                final IColorActionBarFragment to = (IColorActionBarFragment) pagerAdapter.getItem(position + 1);
                // Blend the colors and adjust the ActionBar
                final int blended = blendColors(to.getColor(), from.getColor(), positionOffset);
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
                    actionBarBackground.setColor(blended);
                }
                //getSupportActionBar().setBackgroundDrawable(actionBarBackground);
                mSlidingTabLayout.setSelectedIndicatorColors(blended);
                if (mPlayerControlsFragment != null) {
                    mPlayerControlsFragment.setBackgroundColor(blended);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mWindow.setStatusBarColor(blended);
                    mWindow.setNavigationBarColor(blended);
                }
            }
        });

        // Buffering streaming
        if (activityReceiver != null) {
            // Create an intent filter to listen to the broadcast sent with the
            // action "ACTION_STRING_ACTIVITY"
            IntentFilter intentFilter = new IntentFilter(Constants.ACTION_STRING_ACTIVITY);
            // Map the intent filter to the receiver
            registerReceiver(activityReceiver, intentFilter);
        }

        // Load ads
        AdView adView = (AdView) this.findViewById(R.id.adView1);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("11BBFD42701573FAAAC57C116A029D96")
                .build();
        adView.loadAd(adRequest);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(activityReceiver);
    }

    boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (RadioService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    // ===================== Misc methods =====================

    public void gotowebpage(View v) {
        Intent intent = new Intent();
        intent.setData(Uri.parse("http://www.radiocondado.com"));
        intent.setAction(Intent.ACTION_VIEW);
        startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        final int transStatus = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS;
        if (on) {
            winParams.flags |= transStatus;
        } else {
            winParams.flags &= ~transStatus;
        }
        win.setAttributes(winParams);
    }

}