package es.pulimento.radiocondado.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import java.lang.ref.WeakReference;

import es.pulimento.radiocondado.HomeActivity;
import es.pulimento.radiocondado.R;

public class EnableWifiDialog extends AlertDialog implements OnClickListener {

    private Activity mActivity;

    public EnableWifiDialog(Context context, WeakReference<Activity> activity) {
        super(context);
        mActivity = activity.get();
        this.setTitle(R.string.mainactivity_ask_dialog_title);
        this.setMessage(context.getText(R.string.mainactivity_ask_dialog_msg));
        this.setButton(BUTTON_POSITIVE,
                context.getText(R.string.mainactivity_ask_dialog_yes_button),
                this);
        this.setButton(BUTTON_NEGATIVE,
                context.getText(R.string.mainactivity_ask_dialog_no_button),
                this);
        this.setCancelable(false);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case BUTTON_POSITIVE:
                Intent intent = new Intent(mActivity, HomeActivity.class);
                mActivity.startActivity(intent);
                mActivity.finish();
                break;
            case BUTTON_NEGATIVE:
                mActivity.finish();
                break;
        }

    }

}