package es.pulimento.radiocondado.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javi Pulido on 14/12/2014.
 */
public class RCPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> mFragments = null;

    public RCPagerAdapter(FragmentManager fm) {
        super(fm);
        mFragments = new ArrayList<Fragment>();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return " PORTADA ";
        } else if (position == 1) {
            return " TWITTER ";
        } else if (position == 2) {
            return " FACEBOOK ";
        }
        return "";
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    public void addFragment(Fragment fragment) {
        mFragments.add(fragment);
        notifyDataSetChanged();
    }


}
