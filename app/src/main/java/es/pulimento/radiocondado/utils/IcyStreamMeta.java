package es.pulimento.radiocondado.utils;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IcyStreamMeta {

    protected URL streamUrl;
    private Map<String, String> metadata;
    private boolean isError;

    public IcyStreamMeta(URL streamUrl) {
        setStreamUrl(streamUrl);

        isError = false;
    }

    public static Map<String, String> parseMetadata(String metaString) {
        Map<String, String> metadata = new HashMap<String, String>();
        String[] metaParts = metaString.split(";");
        Pattern p = Pattern.compile("^([a-zA-Z]+)=\\'([^\\']*)\\'$");
        Matcher m;
        for (int i = 0; i < metaParts.length; i++) {
            m = p.matcher(metaParts[i]);
            if (m.find()) {
                metadata.put(m.group(1), m.group(2));
            }
        }

        return metadata;
    }

    /**
     * Get artist using stream's title
     *
     * @return String
     * @throws IOException
     */
    public String getArtist() {

        //String failsafe = "Artista desconocido";
        String failsafe = "";

        Map<String, String> data;
        try {
            data = getMetadata();
        } catch (IOException e) {
            e.printStackTrace();
            return failsafe;
        }

        if (!data.containsKey("StreamTitle"))
            return failsafe;

        try {
            String streamTitle = data.get("StreamTitle");
            String title = streamTitle.substring(0, streamTitle.indexOf("-"));
            return title.trim();
        } catch (Exception e) {
            e.printStackTrace();
            return failsafe;
        }
    }

    /**
     * Get title using stream's title
     *
     * @return String
     * @throws IOException
     */
    public String getTitle() {

        String failsafe = "";
        //String failsafe = "Título desconocido";

        Map<String, String> data;
        try {
            data = getMetadata();
        } catch (IOException e) {
            e.printStackTrace();
            return failsafe;
        }

        if (!data.containsKey("StreamTitle"))
            return failsafe;

        try {
            String streamTitle = data.get("StreamTitle");
            String artist = streamTitle.substring(streamTitle.indexOf("-") + 1);
            return artist.trim();
        } catch (Exception e) {
            e.printStackTrace();
            return failsafe;
        }
    }

    public Map<String, String> getMetadata() throws IOException {
        if (metadata == null) {
            refreshMeta();
        }

        return metadata;
    }

//	private void retreiveMetadata() throws IOException {
//        // Code commented below is old, using stock internal OKHTTP
//
//        /*
//		URLConnection con = streamUrl.openConnection();
//		con.setRequestProperty("Icy-MetaData", "1");
//		con.setRequestProperty("Connection", "close");
//		//con.setRequestProperty("Accept", null);
//		con.connect();
//		*/
//
//        OkHttpClient client = new OkHttpClient();
//        Request request = new Request.Builder()
//                .url(streamUrl)
//                .addHeader("Icy-MetaData", "1")
//                .addHeader("Connection", "close")
//                //.addHeader("Accept", null)
//                .build();
//
//        Response response = client.newCall(request).execute();
//        InputStream stream = response.body().byteStream();
//        String headerReceived = response.header("icy-metaint");
//
//
//		int metaDataOffset = 0;
//        /*
//		Map<String, List<String>> headers = con.getHeaderFields();
//		InputStream stream = con.getInputStream();
//		*/
//
//        /*
//		if (headers.containsKey("icy-metaint")) {
//			// Headers are sent via HTTP
//			metaDataOffset = Integer.parseInt(headers.get("icy-metaint").get(0));
//			*/
//        if(headerReceived != null) {
//            metaDataOffset = Integer.parseInt(headerReceived);
//		} else {
//			// Headers are sent within a stream
//			StringBuilder strHeaders = new StringBuilder();
//			char c;
//			while ((c = (char) stream.read()) != -1) {
//				strHeaders.append(c);
//				if (strHeaders.length() > 5
//						&& (strHeaders.substring((strHeaders.length() - 4), strHeaders.length())
//								.equals("\r\n\r\n"))) {
//					// end of headers
//					break;
//				}
//			}
//
//			// Match headers to get metadata offset within a stream
//			Pattern p = Pattern.compile("\\r\\n(icy-metaint):\\s*(.*)\\r\\n");
//			Matcher m = p.matcher(strHeaders.toString());
//			if (m.find()) {
//				metaDataOffset = Integer.parseInt(m.group(2));
//			}
//		}
//
//		// In case no data was sent
//		if (metaDataOffset == 0) {
//			isError = true;
//			return;
//		}
//
//		// Read metadata
//		int b;
//		int count = 0;
//		int metaDataLength = 4080; // 4080 is the max length
//		boolean inData = false;
//		StringBuilder metaData = new StringBuilder();
//		// Stream position should be either at the beginning or right after
//		// headers
//		while ((b = stream.read()) != -1) {
//			count++;
//
//			// Length of the metadata
//			if (count == metaDataOffset + 1) {
//				metaDataLength = b * 16;
//			}
//
//			if (count > metaDataOffset + 1 && count < (metaDataOffset + metaDataLength)) {
//				inData = true;
//			} else {
//				inData = false;
//			}
//			if (inData) {
//				if (b != 0) {
//					metaData.append((char) b);
//				}
//			}
//			if (count > (metaDataOffset + metaDataLength)) {
//				break;
//			}
//
//		}
//
//		// Set the data
//		metadata = IcyStreamMeta.parseMetadata(metaData.toString());
//
//		// Close
//		stream.close();
//	}

    public void refreshMeta() throws IOException {
        //retreiveMetadata();
    }

    public boolean isError() {
        return isError;
    }

    public URL getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(URL streamUrl) {
        this.metadata = null;
        this.streamUrl = streamUrl;
        this.isError = false;
    }
}