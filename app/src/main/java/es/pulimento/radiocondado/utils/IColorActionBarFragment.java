package es.pulimento.radiocondado.utils;

/**
 * Created by Javi Pulido on 14/12/2014.
 */
public interface IColorActionBarFragment {

    int getColor();
}
