package es.pulimento.radiocondado;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import java.lang.ref.WeakReference;

import es.pulimento.radiocondado.utils.EnableWifiDialog;

/**
 * Simple splash screen that is used to check some pre-requisites before
 * running.
 */
public class InitActivity extends Activity {

    private Activity mActivity;
    private EnableWifiDialog mEnableWifiDialog;
    private NetworkInfo info;
    private ConnectivityManager mConnectivity;
    private String type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        // Define elements
        mActivity = this;
        mEnableWifiDialog = new EnableWifiDialog(mActivity, new WeakReference<Activity>(mActivity));

        mConnectivity = (ConnectivityManager) InitActivity.this
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        info = mConnectivity.getActiveNetworkInfo();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (info != null) {
            type = info.getTypeName();
            if ("mobile".equals(type)) {
                mEnableWifiDialog.show();
            } else {
                nextActivityAndFinish();
            }
        } else {
            nextActivityAndFinish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Dismiss dialogs.
        mEnableWifiDialog.dismiss();
    }

    void nextActivityAndFinish() {
        Intent intent = new Intent(InitActivity.this, HomeActivity.class);
        startActivity(intent);
        this.finish();
    }

}