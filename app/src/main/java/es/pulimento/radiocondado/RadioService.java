package es.pulimento.radiocondado;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.io.IOException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import es.pulimento.radiocondado.utils.Constants;
import es.pulimento.radiocondado.utils.IcyStreamMeta;

public class RadioService extends Service implements MediaPlayer.OnPreparedListener {

    // TUTORIAL http://marakana.com/forums/android/examples/60.html
    // interesting ... http://forum.xda-developers.com/showthread.php?t=1077188
    // media player guide
    // http://developer.android.com/guide/topics/media/mediaplayer.html

    // TODO make a local broadcast receive here to control playback

    private final IBinder mBinder = new MyBinder();
    private MediaPlayer player;
    private NotificationManager mNM;

    @Override
    public void onCreate() {
        Log.d(Constants.TAG, "Service onCreate");

        showPlayerNotification(createNotification());

        player = new MediaPlayer();
        player.setOnPreparedListener(this);
        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        sendPrepared(false);
        try {
            player.setDataSource(Constants.STREAMING_URL);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Log.d(Constants.TAG, "Calling PrepareAsync!");
            player.prepareAsync();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        Log.d(Constants.TAG, "Service onDestroy");
        sendPrepared(false);
        dismissPlayerNotification();
        player.pause();
        player.stop();
        player.release();
        player = null;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        sendPrepared(false);
        dismissPlayerNotification();
        return super.onUnbind(intent);
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Log.i(Constants.TAG, "MediaPlayer onPrepared!!");
        mp.start();
        sendPrepared(true);

        //periodicallyFetchMeta();
    }

    // We return the binder class upon a call of bindService
    @Override
    public IBinder onBind(Intent arg0) {
        return mBinder;
    }

    // send broadcast from activity to all receivers listening to the action
    // "ACTION_STRING_ACTIVITY"
    private void sendPrepared(boolean prepared) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Constants.ACTION_STRING_ACTIVITY);
        broadcastIntent.putExtra(Constants.INTENT_EXTRA_IS_PREPARED, prepared);
        sendBroadcast(broadcastIntent);
    }

    private void sendMeta(String[] meta) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Constants.ACTION_STRING_ACTIVITY);
        String s = Constants.META_INFO_DEFAULT;
        s = meta[0] + " - " + meta[1];
        broadcastIntent.putExtra(Constants.INTENT_SEND_META, s);
        sendBroadcast(broadcastIntent);
    }

    Notification.Builder createNotification() {
        Notification.Builder mBuilder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_notification).setContentTitle("Escuchando ahora")
                .setContentText("Radio Condado 107.4 FM");
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(RadioService.this, HomeActivity.class);

        // The stack builder object will contain an artificial back stack for
        // the started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(HomeActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        return mBuilder;
    }

    void showPlayerNotification(Notification.Builder notificationBuilder) {
        if (mNM == null)
            mNM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = notificationBuilder.build();
        notification.flags |= Notification.FLAG_ONGOING_EVENT;
        mNM.notify(Constants.NOTIFICATION_PLAYER, notification);
    }

    void dismissPlayerNotification() {
        if (mNM == null)
            mNM = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNM.cancel(Constants.NOTIFICATION_PLAYER);
    }

    public void periodicallyFetchMeta() {
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            new MetadataTask().execute(new URL(Constants.STREAMING_URL));
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        if (player.isPlaying())
            timer.schedule(doAsynchronousTask, 0, 50000); // 50 s
    }

    public class MyBinder extends Binder {
        RadioService getService() {
            return RadioService.this;
        }
    }

    protected class MetadataTask extends AsyncTask<URL, Void, String[]> {
        protected IcyStreamMeta streamMeta;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(Constants.TAG, "Starting metadata task");
        }

        @Override
        protected String[] doInBackground(URL... urls) {
            streamMeta = new IcyStreamMeta(urls[0]);
            try {
                streamMeta.refreshMeta();
            } catch (IOException e) {
                // TODO: Handle
                Log.e(MetadataTask.class.toString(), e.getMessage());
            }

            if (streamMeta != null) {
                String title = streamMeta.getArtist();
                String artist = streamMeta.getTitle();
                String[] result = new String[]{title, artist};

                return result;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String[] result) {
            try {
                Log.d(Constants.TAG, "Artist ->" + result[0]);
                Log.d(Constants.TAG, "Title ->" + result[1]);
                sendMeta(result);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
