package es.pulimento.radiocondado.fragments;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import es.pulimento.radiocondado.R;
import es.pulimento.radiocondado.RadioService;
import es.pulimento.radiocondado.utils.Constants;

public class PlayerControlsFragment extends Fragment implements View.OnClickListener {

    private ImageButton mPlayPauseButton;
    private TextView mScrollText;
    private FrameLayout mContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_player_controls, container, false);

        // Bind views
        mPlayPauseButton = (ImageButton) rootView.findViewById(R.id.btn_controls_playpause);
        mScrollText = (TextView) rootView.findViewById(R.id.tv_controls_text);
        mContainer = (FrameLayout) rootView.findViewById(R.id.ll_player_controls_containter);

        // Set OnClickListeners
        //rootView.findViewById(R.id.btn_controls_volume_up).setOnClickListener(this);
        //rootView.findViewById(R.id.btn_controls_volume_down).setOnClickListener(this);
        mPlayPauseButton.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isMyServiceRunning()) {
            mPlayPauseButton.setImageResource(R.drawable.ic_pause_states);
        } else {
            mPlayPauseButton.setImageResource(R.drawable.ic_play_states);
        }
    }

    public void playpause() {
        Intent intent = new Intent(getActivity(), RadioService.class);
        getActivity().startService(intent);
        Toast.makeText(getActivity(), "Iniciando la radio...", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getActivity(), RadioService.class);
        //AudioManager am = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        switch (v.getId()) {
            case R.id.btn_controls_playpause:
                if (isMyServiceRunning()) {
                    getActivity().stopService(intent);
                    Toast.makeText(getActivity(), "Parando la radio...", Toast.LENGTH_SHORT).show();
                    mPlayPauseButton.setImageResource(R.drawable.ic_play_states);
                    sendBroadcast(false);
                } else {
                    getActivity().startService(intent);
                    Toast.makeText(getActivity(), "Iniciando la radio...", Toast.LENGTH_SHORT).show();
                    mPlayPauseButton.setImageResource(R.drawable.ic_pause_states);
                }
                break;
        /*
        case R.id.btn_controls_volume_up:
			am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_RAISE,
					AudioManager.FLAG_SHOW_UI);
			break;
		case R.id.btn_controls_volume_down:
			am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_LOWER,
					AudioManager.FLAG_SHOW_UI);
			break;
		*/
        }
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (RadioService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    // send broadcast from activity to all receivers listening to the action
    // "ACTION_STRING_ACTIVITY"
    private void sendBroadcast(boolean prepared) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(Constants.ACTION_STRING_ACTIVITY);
        broadcastIntent.putExtra(Constants.INTENT_EXTRA_IS_PREPARED, prepared);
        getActivity().sendBroadcast(broadcastIntent);
    }

    public void modifyMetadataText(String newText) {
        try {
            mScrollText.setText(newText);
        } catch (NullPointerException npe) {
            Log.e(Constants.TAG, "Oh no!! One kitty killed :(");
        }
    }

    public void setBackgroundColor(int color) {
        mContainer.setBackgroundColor(color);
    }
}
