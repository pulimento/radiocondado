package es.pulimento.radiocondado.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import es.pulimento.radiocondado.R;
import es.pulimento.radiocondado.utils.Constants;
import es.pulimento.radiocondado.utils.IColorActionBarFragment;

public class InfoFragment extends Fragment implements IColorActionBarFragment {

    /**
     * Empty constructor as per the {@link Fragment} docs
     */
    public InfoFragment() {
    }

    public static InfoFragment newInstance(int color) {
        final Bundle args = new Bundle();
        args.putInt(Constants.KEY_COLOR, color);
        final InfoFragment fragment = new InfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		/* create view and return it */
        View inflatedView = inflater.inflate(R.layout.fragment_info, container, false);

        int res = -1;
        long modulus = System.currentTimeMillis() % 3;
        if (modulus == 0) {
            res = R.drawable.bg_main1;
        } else if (modulus == 1) {
            res = R.drawable.bg_main2;
        } else {
            res = R.drawable.bg_portada;
        }
        ((ImageView) inflatedView.findViewById(R.id.iv_info_center)).setImageResource(res);
        inflatedView.findViewById(R.id.iv_info_center).setBackgroundColor(0x000000);
        // ((ImageView)
        // inflatedView.findViewById(R.id.iv_info_center)).setScaleType(ScaleType.CENTER);

        return inflatedView;
    }

    public int getColor() {
        return getArguments().getInt(Constants.KEY_COLOR);
    }

    // ===================== Menus stuff ============================
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_fragment_info, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {
            case R.id.menu_fragment_tweet_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getText(R.string.intent_share_message));
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent,
                        getResources().getText(R.string.intent_share_chooser_title)));
                return true;
            default:
                return false;
        }
    }

}
