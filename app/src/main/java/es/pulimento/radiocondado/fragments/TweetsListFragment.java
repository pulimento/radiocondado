package es.pulimento.radiocondado.fragments;


import android.app.Activity;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import es.pulimento.radiocondado.R;
import es.pulimento.radiocondado.utils.Constants;
import es.pulimento.radiocondado.utils.IColorActionBarFragment;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TweetsListFragment extends ListFragment implements IColorActionBarFragment {

    private static final String tweetsSearchQuery = "radiocondado";
    Twitter mTwitter;
    FloatingActionButton mFAB;
    ListAdapter mListAdapter;
    List<Status> mTweetsList;
    ListView mListView;
    //PullRefreshLayout mPullToRefresh;
    SwipeRefreshLayout mPullToRefres;
    TextView mEmptyView;

    /**
     * Empty constructor as per the {@link android.support.v4.app.Fragment} docs
     */
    public TweetsListFragment() {
    }

    public static TweetsListFragment newInstance(int color) {
        final Bundle args = new Bundle();
        args.putInt(Constants.KEY_COLOR, color);
        final TweetsListFragment fragment = new TweetsListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tweetslist, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Give values to fields
        mTweetsList = new ArrayList<Status>();
        mListView = getListView();
        mEmptyView = (TextView) getActivity().findViewById(R.id.tv_tweets_empty_view);

        // Show loading view, hide list
        mEmptyView.setVisibility(View.VISIBLE);
        mListView.setVisibility(View.GONE);

        // Set custom list adapter
        mListAdapter = new TweetsListAdapter(mTweetsList, getActivity());
        setListAdapter(mListAdapter);

        // Set pull to refresh
        //mPullToRefresh = (PullRefreshLayout) getActivity().findViewById(R.id.ptr_tweets_list);

        mPullToRefres = (SwipeRefreshLayout) getActivity().findViewById(R.id.ptr_tweets_list);

        mPullToRefres.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showTweetsAbout(tweetsSearchQuery, false);
            }
        });

        /*mPullToRefresh.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showTweetsAbout(tweetsSearchQuery, false);
            }
        });
        */

        // Attach FAB
        mFAB = (FloatingActionButton) getActivity().findViewById(R.id.fab_tweets_list);
        mFAB.attachToListView(mListView);
        mFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://twitter.com/intent/tweet?hashtags=RadioCondado&lang=es";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        // Gives value to mTwitter
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setOAuthConsumerKey(Constants.TWITTER_CONSUMER_KEY);
        cb.setOAuthConsumerSecret(Constants.TWITTER_CONSUMER_SECRET);
        cb.setOAuthAccessToken(Constants.TWITTER_ACCESS_TOKEN);
        cb.setOAuthAccessTokenSecret(Constants.TWITTER_ACCESS_TOKEN_SECRET);
        TwitterFactory tf = new TwitterFactory(cb.build());
        mTwitter = tf.getInstance();

        // Launches the async task
        showTweetsAbout(tweetsSearchQuery, true);
    }

    private void showTweetsAbout(String queryString, boolean hideShowedTweets) {
        new showTweetsAboutTask(hideShowedTweets).execute(queryString);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.d("RadioCondado", "Tweet on list clicked!");

        final Status t = (Status) mListAdapter.getItem(position);
        if (t != null) {

            String tweetURL = String.format(getActivity().getString(R.string.intent_twitter_single_tweet), t
                    .getUser().getScreenName(), t.getId());
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetURL));
            startActivity(browserIntent);
        } else {
            Toast.makeText(getActivity(), "Ha habido un error al mostrar el tweet", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    public int getColor() {
        return getArguments().getInt(Constants.KEY_COLOR);
    }

    private class showTweetsAboutTask extends AsyncTask<String, Void, List<Status>> {

        Boolean hidePreviousTweets;

        public showTweetsAboutTask(Boolean hidePreviousTweets) {
            this.hidePreviousTweets = hidePreviousTweets;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (hidePreviousTweets) {
                mEmptyView.setVisibility(View.VISIBLE);
                mListView.setVisibility(View.GONE);
            }
        }

        @Override
        protected List<twitter4j.Status> doInBackground(String... params) {

            Query query = new Query(params[0]);
            query.setCount(40);
            QueryResult result = null;
            try {
                result = mTwitter.search(query);
            } catch (TwitterException e1) {
                e1.printStackTrace();
            }

            if (result != null) {

                if (mTweetsList == null) {
                    mTweetsList = new ArrayList<twitter4j.Status>();
                } else {
                    mTweetsList.clear();
                }

                return result.getTweets();
            } else
                return null;
        }

        @Override
        protected void onPostExecute(List<twitter4j.Status> result) {
            if (result != null) {
                if (!mTweetsList.isEmpty())
                    mTweetsList.clear();
                mTweetsList.addAll(result);
                try {
                    mListView.invalidateViews();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mListView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
            } else {
                mListView.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);
                mEmptyView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTweetsAbout(tweetsSearchQuery, true);
                    }
                });
                //Toast.makeText(getActivity(), "Error al obtener los tweets. ¿Hay conexión a internet?",
                //        Toast.LENGTH_LONG).show();
            }

            mPullToRefres.setRefreshing(false);
        }
    }

    class TweetsListAdapter implements ListAdapter {

        /* Variables. */
        private List<Status> mItems;
        private LayoutInflater mLayoutInflater;
        private ImageView avatar;

        public TweetsListAdapter(List<Status> items, Activity act) {

			/* Initialize all variables. */
            // Testing if this force listview to update
            // mItems = new ArrayList<Tweet>();
            mItems = items;
            mItems.add(null);

            mLayoutInflater = LayoutInflater.from(getActivity());
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = mLayoutInflater.inflate(R.layout.row_tweet, null);
            }
            Status item = mItems.get(position);
            if (item != null) {
                avatar = (ImageView) v.findViewById(R.id.listview_item_tweet_avatar);

                Ion.with(avatar).placeholder(R.drawable.ic_launcher).load(item.getUser().getBiggerProfileImageURL());

                TextView essid = (TextView) v.findViewById(R.id.listview_item_tweet_text);
                if (essid != null)
                    essid.setText(item.getText());
                TextView bssid = (TextView) v.findViewById(R.id.listview_item_tweet_user);
                if (bssid != null)
                    bssid.setText("@" + item.getUser().getScreenName());
            }
            return v;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isEmpty() {
            return mItems.isEmpty();
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {
        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

    }
}